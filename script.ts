import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const main = async () => {
  const users = await prisma.users.findMany({
    where: {
      email: {
        contains: 'm',
      },
    },
  });

  await prisma.users.create({
    data: {
      name: 'Jojo Koko',
      email: 'jojo@email.com',
    },
  });

  console.dir(users, { depth: 'infinity' });
};

main()
  .catch((e) => {
    throw e;
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
